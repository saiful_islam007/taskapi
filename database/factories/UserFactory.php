<?php

namespace Database\Factories;

use App\Models\Person;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{

    
    /**
     * Define the model's default state.
     * 
     * 
     *
     * @return array<string, mixed>
     */



    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'age'=>$this->faker->numberBetween($min = 1, $max = 100),
            'bio'=>$this->faker->realText($maxNbChars = 200, $indexSize = 2),

           
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
  
}